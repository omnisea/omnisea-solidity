// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

struct ERC1155CreateParams {
    string name;
    string symbol;
    string uri;
    string[] tokenURIs;
    uint256[] maxSupply;
    uint24 royaltyAmount;
    uint256 endTime;
    bool isSBT;
    uint256[] premintQuantity;
}

struct ERC1155MintParams {
    address to;
    address collection;
    uint256 tokenId;
    uint24 quantity;
    bytes32[] merkleProof;
    uint8 phaseId;
    bytes payloadForCall;
}

struct ERC1155OmnichainMintParams {
    address collection;
    uint256 tokenId;
    uint24 quantity;
    uint256 paid;
    uint8 phaseId;
    address minter;
}

struct ERC1155Phase {
    uint256 from;
    uint256 to;
    uint24 maxPerAddress;
    uint256 price;
    bytes32 merkleRoot;
    address token;
    uint256 minToken;
}
