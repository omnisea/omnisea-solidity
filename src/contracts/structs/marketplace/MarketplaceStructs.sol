// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

struct CreateListingParams {
    uint256 endTime;
}

struct FillListingParams {
    address listing;
    address receiver;
    bytes32[] merkleProof;
    uint8 phaseId;
}

struct Phase {
    uint256 from;
    uint256 to;
    uint256 price;
    bytes32 merkleRoot;
    address token;
    uint256 minToken;
}
