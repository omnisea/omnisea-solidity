// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

struct InscriptionMintParams {
    address to;
    address collection;
    uint24 quantity;
    bytes32[] merkleProof;
    uint8 phaseId;
    bytes payloadForCall;
}

struct InscriptionPhase {
    uint256 from;
    uint256 to;
    uint24 maxPerAddress;
    uint256 price;
    bytes32 merkleRoot;
    address token;
    uint256 minToken;
}
