// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

enum AssetType {
    ERC721,
    ERC1155,
    ERC20
}

struct PackCreateParams {
    uint256 endTime;
    uint256 claimableFrom;
}

struct ReserveParams {
    address to;
    address pack;
    uint24 quantity;
    bytes32[] merkleProof;
    uint8 phaseId;
}

struct Phase {
    uint256 from;
    uint256 to;
    uint24 maxPerAddress;
    uint256 price;
    bytes32 merkleRoot;
    address token;
    uint256 minToken;
}
